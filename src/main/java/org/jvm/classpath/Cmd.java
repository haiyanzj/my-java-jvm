package org.jvm.classpath;

import org.apache.commons.cli.CommandLine;
import org.apache.commons.cli.CommandLineParser;
import org.apache.commons.cli.DefaultParser;
import org.apache.commons.cli.Options;

/**
 * author : wangsixiang02
 * date : 2022/12/31
 * desc :
 */
public class Cmd {

    private Boolean helpFlag;
    private Boolean versionFlag;
    private String cpOption;
    private String XjreOption;
    private String className;
    private String[] args;

    public Boolean getHelpFlag() {
        return helpFlag;
    }

    public void setHelpFlag(Boolean helpFlag) {
        this.helpFlag = helpFlag;
    }

    public Boolean getVersionFlag() {
        return versionFlag;
    }

    public void setVersionFlag(Boolean versionFlag) {
        this.versionFlag = versionFlag;
    }

    public String getCpOption() {
        return cpOption;
    }

    public void setCpOption(String cpOption) {
        this.cpOption = cpOption;
    }

    public String getXjreOption() {
        return XjreOption;
    }

    public void setXjreOption(String xjreOption) {
        XjreOption = xjreOption;
    }

    public String getClassName() {
        return className;
    }

    public void setClassName(String className) {
        this.className = className;
    }

    public String[] getArgs() {
        return args;
    }

    public void setArgs(String[] args) {
        this.args = args;
    }

    public Cmd(String[] cmdLine) {
        Options options = new Options();
        options.addOption("help", false, "print help message");
        options.addOption("?", false, "print help message");
        options.addOption("version", false, "print version and exit");
        options.addOption("cp", "classPath", true, "classpath");
        options.addOption("Xjre", true, "path to jre");
        CommandLineParser parser = new DefaultParser();
        CommandLine cmd = null;
        try {
            cmd = parser.parse(options, cmdLine);
        } catch (Exception e) {
            e.printStackTrace();
        }
        this.cpOption = cmd.getOptionValue("cp");
        this.XjreOption = cmd.getOptionValue("Xjre");
        this.helpFlag = cmd.hasOption("help") || cmd.hasOption("?");
        this.versionFlag = cmd.hasOption("version");
        if (cmd.getArgs() != null && cmd.getArgs().length > 0) {
            className = cmd.getArgs()[0];
            if (cmd.getArgs().length > 1) {
                this.args = new String[cmd.getArgs().length - 1];
                for (int i = 1; i < cmd.getArgs().length; i++) {
                    this.args[i - 1] = cmd.getArgs()[i];
                }
            }
        }
    }
}