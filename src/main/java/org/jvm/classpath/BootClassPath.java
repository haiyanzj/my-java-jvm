package org.jvm.classpath;

import org.jvm.classpath.entry.Entry;
import org.jvm.classpath.entry.EntryBuilder;

import java.io.File;

/**
 * Java根类路径
 *
 * @author 海燕
 * @date 2023/1/5
 */
public class BootClassPath {

    /**
     * 根类路径
     */
    private final Entry bootClassPath;

    /**
     * 从根类路径读取类
     *
     * @param className
     * @return
     */
    public byte[] readClass(String className) {
        return bootClassPath.readClass(className.concat(".class"));
    }

    /**
     * @param jreOption 根类路径
     */
    public BootClassPath(String jreOption) {
        String jreDir = getJreDir(jreOption);
        this.bootClassPath = EntryBuilder.newEntry(jreDir + File.separator + "lib" + File.separator + "*");
    }

    /**
     * 首先使用用户指定的jre目录
     * 其次如果用户没有指定，则使用当前目录
     * 再次如果当前目录不存在，使用系统变量配置的JAVA_HOME
     *
     * @param jreOption
     * @return
     */
    private String getJreDir(String jreOption) {
        if (jreOption != null && jreOption.length() > 0 && dirExists(jreOption)) {
            return jreOption;
        }
        if (dirExists("./jre")) {
            return "./jre";
        }
        return System.getenv("JAVA_HOME");
    }

    private Boolean dirExists(String dir) {
        File file = new File(dir);
        return file.exists();
    }
}
