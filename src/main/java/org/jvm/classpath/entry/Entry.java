package org.jvm.classpath.entry;


public interface Entry {

    String pathListSeparator = ";";

    /**
     * 读取.class字节码
     *
     * @param className
     * @return
     */
    byte[] readClass(String className);

    String getPath();
}
