package org.jvm.classpath.entry;

public class EntryBuilder {

    public static Entry newEntry(String path) {
        if (path.contains(Entry.pathListSeparator) || path.endsWith("*")) {
            return new CompositeEntry(path);
        }
        if (path.toLowerCase().endsWith(".jar")) {
            return new ZipEntry(path);
        }
        return new DirEntry(path);
    }
}

