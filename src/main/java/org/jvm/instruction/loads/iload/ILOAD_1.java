package org.jvm.instruction.loads.iload;

import org.jvm.instruction.base.NoOperandsInstruction;
import org.jvm.rtda.thread.Frame;

/**
 * 同ILOAD指令，隐含操作数
 *
 * @author 海燕
 * @date 2023/1/14
 */
public class ILOAD_1 extends NoOperandsInstruction {
    @Override
    public void execute(Frame frame) {
        frame.getOperandStack().pushInt(frame.getLocalVars().getInt(1));
    }
}
