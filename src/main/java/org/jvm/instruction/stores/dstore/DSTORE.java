package org.jvm.instruction.stores.dstore;

import org.jvm.instruction.base.Index8Instruction;
import org.jvm.rtda.thread.Frame;

/**
 * 存储指令，将操作数栈弹栈并存入本地变量表
 *
 * @author 海燕
 * @date 2023/1/14
 */
public class DSTORE extends Index8Instruction {
    @Override
    public void execute(Frame frame) {
        frame.getLocalVars().setDouble(this.index, frame.getOperandStack().popDouble());
    }
}
