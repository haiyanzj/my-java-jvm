package org.jvm.instruction.control;

import org.jvm.instruction.base.BRANCH;
import org.jvm.rtda.thread.Frame;

/**
 * goto指令，执行无条件跳转
 *
 * @author 海燕
 * @date 2023/1/29
 */
public class GOTO extends BRANCH {

    @Override
    protected boolean getCond(Frame frame) {
        return true;
    }
}
