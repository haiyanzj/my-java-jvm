package org.jvm.instruction.control.ret;

import org.jvm.instruction.base.NoOperandsInstruction;
import org.jvm.rtda.thread.Frame;
import org.jvm.rtda.thread.Thread;

/**
 * @author 海燕
 * @date 2023/2/11
 */
public class IRETURN extends NoOperandsInstruction {
    @Override
    public void execute(Frame frame) {
        Thread thread = frame.getThread();
        Frame thisFrame = thread.popFrame();
        //线程中弹出一个栈帧后，拿到上一个栈帧，这个栈帧接收方法返回值
        Frame previousFrame = thread.currentFrame();
        //从当前栈帧中弹出返回值，压入上个栈帧的操作数栈
        int i = thisFrame.getOperandStack().popInt();
        previousFrame.getOperandStack().pushInt(i);
    }
}
