package org.jvm.instruction.control.ret;

import org.jvm.instruction.base.NoOperandsInstruction;
import org.jvm.rtda.thread.Frame;

/**
 * void方法return指令
 *
 * @author 海燕
 * @date 2023/2/11
 */
public class RETURN extends NoOperandsInstruction {
    @Override
    public void execute(Frame frame) {
        //单纯弹出一个栈帧，不做任何其他操作
        frame.getThread().popFrame();
    }
}
