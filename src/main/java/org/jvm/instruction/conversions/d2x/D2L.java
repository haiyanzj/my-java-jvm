package org.jvm.instruction.conversions.d2x;

import org.jvm.instruction.base.NoOperandsInstruction;
import org.jvm.rtda.thread.Frame;

/**
 * 类型转换指令
 *
 * @author 海燕
 * @date 2023/1/15
 */
public class D2L extends NoOperandsInstruction {
    @Override
    public void execute(Frame frame) {
        double v1 = frame.getOperandStack().popDouble();
        frame.getOperandStack().pushLong((long) v1);
    }
}
