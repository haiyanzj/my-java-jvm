package org.jvm.instruction.stack.dup;

import org.jvm.instruction.base.NoOperandsInstruction;
import org.jvm.rtda.Slot;
import org.jvm.rtda.thread.Frame;

/**
 * 复制栈顶元素
 *
 * @author 海燕
 * @date 2023/1/15
 */
public class DUP extends NoOperandsInstruction {
    @Override
    public void execute(Frame frame) {
        Slot peek = frame.getOperandStack().popSlot();
        frame.getOperandStack().pushSlot(peek);
        frame.getOperandStack().pushSlot(peek.clone());
    }
}
