package org.jvm.instruction.extended;

import org.jvm.instruction.base.ByteCodeReader;
import org.jvm.instruction.base.Instruction;
import org.jvm.instruction.base.InstructionUtil;
import org.jvm.rtda.thread.Frame;

/**
 * 和goto指令的区别是最大偏移量扩展为4字节
 *
 * @author 海燕
 * @date 2023/1/29
 */
public class GOTO_W implements Instruction {

    /**
     * 程序计数器偏移量
     */
    protected int offset;

    /**
     * @param reader
     */
    @Override
    public void fetchOperands(ByteCodeReader reader) {
        this.offset = reader.readUint32();
    }

    @Override
    public void execute(Frame frame) {
        InstructionUtil.branch(frame, this.offset);
    }
}
