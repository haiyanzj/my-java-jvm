package org.jvm.instruction.constants;

import org.jvm.instruction.base.ByteCodeReader;
import org.jvm.instruction.base.Instruction;
import org.jvm.rtda.thread.Frame;

/**
 * 读取一个short的操作数转换为int，压入操作数栈
 *
 * @author 海燕
 * @date 2023/1/14
 */
public class SIPUSH implements Instruction {

    private int val;

    @Override
    public void fetchOperands(ByteCodeReader reader) {
        this.val = reader.readInt16();
    }

    @Override
    public void execute(Frame frame) {
        frame.getOperandStack().pushInt(this.val);
    }
}
