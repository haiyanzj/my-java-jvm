package org.jvm.instruction.constants;

import org.jvm.instruction.base.NoOperandsInstruction;
import org.jvm.rtda.thread.Frame;

/**
 * 将float 0压操作数栈
 *
 * @author 海燕
 * @date 2023/1/14
 */
public class FCONST_0 extends NoOperandsInstruction {

    @Override
    public void execute(Frame frame) {
        frame.getOperandStack().pushFloat(0f);
    }

}
