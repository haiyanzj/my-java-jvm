package org.jvm.instruction.constants;

import org.jvm.instruction.base.NoOperandsInstruction;
import org.jvm.rtda.thread.Frame;

/**
 * 将int 2压操作数栈
 *
 * @author 海燕
 * @date 2023/1/14
 */
public class ICONST_2 extends NoOperandsInstruction {

    @Override
    public void execute(Frame frame) {
        frame.getOperandStack().pushInt(2);
    }

}
