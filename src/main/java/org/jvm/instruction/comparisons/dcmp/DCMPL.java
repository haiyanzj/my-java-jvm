package org.jvm.instruction.comparisons.dcmp;

import org.jvm.rtda.thread.Frame;

/**
 * 比较指令
 *
 * @author 海燕
 * @date 2023/1/19
 */
public class DCMPL extends DCMP {

    @Override
    public void execute(Frame frame) {
        dcmpWithFlag(frame, false);
    }
}