package org.jvm.instruction.comparisons.branch.ifacmp;

import org.jvm.instruction.base.BRANCH;
import org.jvm.rtda.Object;
import org.jvm.rtda.thread.Frame;

/**
 * @author 海燕
 * @date 2023/1/29
 */
public abstract class IFACMP extends BRANCH {

    /**
     * 弹出栈顶的2个引用，判断2个引用是否相等
     *
     * @return
     */
    @Override
    protected boolean getCond(Frame frame) {
        Object val2 = frame.getOperandStack().popRef();
        Object val1 = frame.getOperandStack().popRef();
        return compareRef(val1, val2);
    }

    protected abstract boolean compareRef(Object val1, Object val2);
}
