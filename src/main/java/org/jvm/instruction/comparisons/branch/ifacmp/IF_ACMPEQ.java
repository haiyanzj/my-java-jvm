package org.jvm.instruction.comparisons.branch.ifacmp;

import org.jvm.rtda.Object;

/**
 * @author 海燕
 * @date 2023/1/29
 */
public class IF_ACMPEQ extends IFACMP {
    @Override
    protected boolean compareRef(Object val1, Object val2) {
        return val1 == val2;
    }
}
