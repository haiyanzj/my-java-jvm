package org.jvm.instruction.math.neg;

import org.jvm.instruction.base.NoOperandsInstruction;
import org.jvm.rtda.thread.Frame;

/**
 * 求反
 *
 * @author 海燕
 * @date 2023/1/15
 */
public class FNEG extends NoOperandsInstruction {
    @Override
    public void execute(Frame frame) {
        float val = frame.getOperandStack().popFloat();
        frame.getOperandStack().pushFloat(-val);
    }
}
