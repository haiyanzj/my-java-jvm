package org.jvm.instruction.math.sub;

import org.jvm.instruction.base.NoOperandsInstruction;
import org.jvm.rtda.thread.Frame;

/**
 * double相减
 *
 * @author 海燕
 * @date 2023/1/15
 */
public class FSUB extends NoOperandsInstruction {
    @Override
    public void execute(Frame frame) {
        float v2 = frame.getOperandStack().popFloat();
        float v1 = frame.getOperandStack().popFloat();
        frame.getOperandStack().pushFloat(v1 - v2);
    }
}
