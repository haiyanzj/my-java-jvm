package org.jvm.instruction.math.sub;

import org.jvm.instruction.base.NoOperandsInstruction;
import org.jvm.rtda.thread.Frame;

/**
 * double相减
 *
 * @author 海燕
 * @date 2023/1/15
 */
public class ISUB extends NoOperandsInstruction {
    @Override
    public void execute(Frame frame) {
        int v2 = frame.getOperandStack().popInt();
        int v1 = frame.getOperandStack().popInt();
        frame.getOperandStack().pushInt(v1 - v2);
    }
}
