package org.jvm.instruction.math.sub;

import org.jvm.instruction.base.NoOperandsInstruction;
import org.jvm.rtda.thread.Frame;

/**
 * double相减
 *
 * @author 海燕
 * @date 2023/1/15
 */
public class LSUB extends NoOperandsInstruction {
    @Override
    public void execute(Frame frame) {
        long v2 = frame.getOperandStack().popLong();
        long v1 = frame.getOperandStack().popLong();
        frame.getOperandStack().pushLong(v1 - v2);
    }
}
