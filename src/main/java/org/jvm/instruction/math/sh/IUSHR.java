package org.jvm.instruction.math.sh;

import org.jvm.instruction.base.NoOperandsInstruction;
import org.jvm.rtda.thread.Frame;

/**
 * int 无符号右移
 *
 * @author 海燕
 * @date 2023/1/15
 */
public class IUSHR extends NoOperandsInstruction {
    @Override
    public void execute(Frame frame) {
        //位移步长
        int step = frame.getOperandStack().popInt();
        int i = frame.getOperandStack().popInt();
        frame.getOperandStack().pushInt(i >>> step);
    }
}
