package org.jvm.instruction.math.sh;

import org.jvm.instruction.base.NoOperandsInstruction;
import org.jvm.rtda.thread.Frame;

/**
 * long 带符号右移
 *
 * @author 海燕
 * @date 2023/1/15
 */
public class LSHR extends NoOperandsInstruction {
    @Override
    public void execute(Frame frame) {
        //位移步长
        int step = frame.getOperandStack().popInt();
        long i = frame.getOperandStack().popLong();
        frame.getOperandStack().pushLong(i >> step);
    }
}
