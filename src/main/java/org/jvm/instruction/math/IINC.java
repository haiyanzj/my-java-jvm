package org.jvm.instruction.math;

import org.jvm.instruction.base.ByteCodeReader;
import org.jvm.instruction.base.Instruction;
import org.jvm.rtda.thread.Frame;

/**
 * 从局部变量表指定索引拿一个int
 * 加上指定的常量int
 * 写回局部变量表原位置
 */
public class IINC implements Instruction {

    /**
     * 局部变量表索引
     */
    private int index;
    /**
     * 常量
     */
    private int constValue;

    @Override
    public void fetchOperands(ByteCodeReader reader) {
        this.index = reader.readUint8();
        this.constValue = reader.readInt8();
    }

    @Override
    public void execute(Frame frame) {
        int val = frame.getLocalVars().getInt(this.index);
        val += this.constValue;
        frame.getLocalVars().setInt(this.index, val);
    }

    public int getIndex() {
        return index;
    }

    public void setIndex(int index) {
        this.index = index;
    }

    public int getConstValue() {
        return constValue;
    }

    public void setConstValue(int constValue) {
        this.constValue = constValue;
    }
}
