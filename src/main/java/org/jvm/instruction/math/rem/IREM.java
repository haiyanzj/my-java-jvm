package org.jvm.instruction.math.rem;

import org.jvm.instruction.base.NoOperandsInstruction;
import org.jvm.rtda.thread.Frame;

/**
 * int求余指令
 *
 * @author 海燕
 * @date 2023/1/15
 */
public class IREM extends NoOperandsInstruction {
    @Override
    public void execute(Frame frame) {
        //被除数dividend  除数divisor
        int divisor = frame.getOperandStack().popInt();
        int dividend = frame.getOperandStack().popInt();
        if (divisor == 0) {
            throw new RuntimeException("除数不能为零");
        }
        frame.getOperandStack().pushInt(dividend % divisor);
    }
}
