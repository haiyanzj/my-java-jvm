package org.jvm.instruction.math.rem;

import org.jvm.instruction.base.NoOperandsInstruction;
import org.jvm.rtda.thread.Frame;

import java.math.BigInteger;

/**
 * long求余指令
 *
 * @author 海燕
 * @date 2023/1/15
 */
public class LREM extends NoOperandsInstruction {
    @Override
    public void execute(Frame frame) {
        //被除数dividend  除数divisor
        long divisor = frame.getOperandStack().popLong();
        long dividend = frame.getOperandStack().popLong();
        if (divisor == 0l) {
            throw new RuntimeException("除数不能为零");
        }
        BigInteger divisorBig = BigInteger.valueOf(divisor);
        BigInteger dividendBig = BigInteger.valueOf(dividend);
        frame.getOperandStack().pushLong(dividendBig.mod(divisorBig).longValue());
    }
}
