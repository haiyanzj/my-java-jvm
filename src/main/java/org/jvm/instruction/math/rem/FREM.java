package org.jvm.instruction.math.rem;

import org.jvm.instruction.base.NoOperandsInstruction;
import org.jvm.rtda.thread.Frame;

/**
 * float求余指令
 *
 * @author 海燕
 * @date 2023/1/15
 */
public class FREM extends NoOperandsInstruction {
    @Override
    public void execute(Frame frame) {
        //被除数dividend  除数divisor
        float divisor = frame.getOperandStack().popFloat();
        float dividend = frame.getOperandStack().popFloat();
        frame.getOperandStack().pushFloat(dividend % divisor);
    }
}
