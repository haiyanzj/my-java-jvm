package org.jvm.instruction.math.mul;

import org.jvm.instruction.base.NoOperandsInstruction;
import org.jvm.rtda.thread.Frame;

/**
 * 乘法指令
 *
 * @author 海燕
 * @date 2023/1/17
 */
public class LMUL extends NoOperandsInstruction {
    @Override
    public void execute(Frame frame) {
        long v1 = frame.getOperandStack().popLong();
        long v2 = frame.getOperandStack().popLong();
        frame.getOperandStack().pushLong(v2 * v1);
    }
}
