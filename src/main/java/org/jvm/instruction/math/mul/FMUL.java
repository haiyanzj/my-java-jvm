package org.jvm.instruction.math.mul;

import org.jvm.instruction.base.NoOperandsInstruction;
import org.jvm.rtda.thread.Frame;

/**
 * 乘法指令
 *
 * @author 海燕
 * @date 2023/1/17
 */
public class FMUL extends NoOperandsInstruction {
    @Override
    public void execute(Frame frame) {
        float v1 = frame.getOperandStack().popFloat();
        float v2 = frame.getOperandStack().popFloat();
        frame.getOperandStack().pushFloat(v2 * v1);
    }
}
