package org.jvm.instruction.math.add;

import org.jvm.instruction.base.NoOperandsInstruction;
import org.jvm.rtda.thread.Frame;

/**
 * float相加指令
 *
 * @author 海燕
 * @date 2023/1/15
 */
public class FADD extends NoOperandsInstruction {
    @Override
    public void execute(Frame frame) {
        float v1 = frame.getOperandStack().popFloat();
        float v2 = frame.getOperandStack().popFloat();
        frame.getOperandStack().pushFloat(v1 + v2);
    }
}
