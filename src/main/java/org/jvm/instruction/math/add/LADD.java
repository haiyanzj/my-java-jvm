package org.jvm.instruction.math.add;

import org.jvm.instruction.base.NoOperandsInstruction;
import org.jvm.rtda.thread.Frame;

/**
 * long相加指令
 *
 * @author 海燕
 * @date 2023/1/15
 */
public class LADD extends NoOperandsInstruction {
    @Override
    public void execute(Frame frame) {
        long v1 = frame.getOperandStack().popLong();
        long v2 = frame.getOperandStack().popLong();
        frame.getOperandStack().pushLong(v1 + v2);
    }
}
