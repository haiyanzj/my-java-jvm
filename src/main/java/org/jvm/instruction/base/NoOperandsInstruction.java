package org.jvm.instruction.base;

/**
 * 用以表示没有操作数或者操作数隐含在指令名中的指令
 *
 * @author 海燕
 * @date 2023/1/14
 */
public abstract class NoOperandsInstruction implements Instruction {

    /**
     * 由于没有操作数，所以这里不用做任何操作
     *
     * @param reader
     */
    @Override
    public void fetchOperands(ByteCodeReader reader) {

    }
}
