package org.jvm.instruction.references.array.astore;

import org.jvm.instruction.base.NoOperandsInstruction;
import org.jvm.rtda.Object;
import org.jvm.rtda.thread.Frame;
import org.jvm.rtda.thread.OperandStack;

/**
 * @author 海燕
 * @date 2023/2/15
 */
public class CASTORE extends NoOperandsInstruction {
    @Override
    public void execute(Frame frame) {
        OperandStack operandStack = frame.getOperandStack();
        char val = (char) operandStack.popInt();
        int index = operandStack.popInt();
        Object arrayRef = operandStack.popRef();
        if (arrayRef == null) {
            frame.getThread().throwNullPointerException(frame.getMethod().getKlass().getKlassLoader());
            return;
        }
        if (index < 0 || index >= arrayRef.arrayLength()) {
            frame.getThread().throwArrayIndexOutOfBoundsException(frame.getMethod().getKlass().getKlassLoader());
            return;
        }
        arrayRef.chars()[index] = val;
    }
}
