package org.jvm.instruction.references.array.aload;

import org.jvm.instruction.base.NoOperandsInstruction;
import org.jvm.rtda.Object;
import org.jvm.rtda.thread.Frame;
import org.jvm.rtda.thread.OperandStack;

/**
 * @author 海燕
 * @date 2023/2/15
 */
public class SALOAD extends NoOperandsInstruction {
    @Override
    public void execute(Frame frame) {
        OperandStack operandStack = frame.getOperandStack();
        int index = operandStack.popInt();
        Object arrayObject = operandStack.popRef();
        if (arrayObject == null) {
            frame.getThread().throwNullPointerException(frame.getMethod().getKlass().getKlassLoader());
            return;
        }
        if (index < 0 || index >= arrayObject.arrayLength()) {
            frame.getThread().throwArrayIndexOutOfBoundsException(frame.getMethod().getKlass().getKlassLoader());
            return;
        }
        short s = arrayObject.shorts()[index];
        operandStack.pushInt(s & 0x0FFFF);
    }
}
