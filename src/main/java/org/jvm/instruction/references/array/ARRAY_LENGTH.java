package org.jvm.instruction.references.array;

import org.jvm.instruction.base.NoOperandsInstruction;
import org.jvm.rtda.Object;
import org.jvm.rtda.thread.Frame;
import org.jvm.rtda.thread.OperandStack;

/**
 * 获取数组长度
 *
 * @author 海燕
 * @date 2023/2/14
 */
public class ARRAY_LENGTH extends NoOperandsInstruction {
    @Override
    public void execute(Frame frame) {
        OperandStack operandStack = frame.getOperandStack();
        Object object = operandStack.popRef();
        if (object == null) {
            frame.getThread().throwNullPointerException(frame.getMethod().getKlass().getKlassLoader());
            return;
        }
        int arrayLength = object.arrayLength();
        operandStack.pushInt(arrayLength);
    }
}
