package org.jvm.instruction.references;

import org.jvm.instruction.base.Index16Instruction;
import org.jvm.rtda.Object;
import org.jvm.rtda.Slots;
import org.jvm.rtda.heap.ConstantPool;
import org.jvm.rtda.heap.Klass;
import org.jvm.rtda.heap.classmember.Field;
import org.jvm.rtda.heap.symref.FieldRef;
import org.jvm.rtda.thread.Frame;
import org.jvm.rtda.thread.OperandStack;

/**
 * 给类的静态变量赋值
 *
 * @author 海燕
 * @date 2023/2/6
 */
public class PUT_FIELD extends Index16Instruction {


    /**
     * 指令操作数含义为类常量池索引
     *
     * @param frame
     */
    @Override
    public void execute(Frame frame) {
        //从代码执行类常量池获取一个字段符号引用
        Klass thisClass = frame.getMethod().getKlass();
        ConstantPool constantPool = thisClass.getConstantPool();
        FieldRef fieldRef = (FieldRef) constantPool.getConstant(this.index);
        //解析被赋值字段符号引用
        Field field = fieldRef.resolvedField(frame.getThread());
        if (field.isStatic()) {
            throw new RuntimeException("字段为静态");
        }
        //被赋值字段所在的类
        Klass thatClass = field.getKlass();
        //被final修饰的实例字段，仅可以在字段本身类的构造方法中赋值
        if (field.isFinal()) {
            if (thisClass != thatClass || !frame.getMethod().getName().equals("<init>")) {
                throw new RuntimeException("被final修饰的字段，仅可以在字段本身类的构造方法中赋值");
            }
        }
        //进行实例对象的赋值，从操作数栈中依次弹出变量值和被赋值的实例对象引用
        String descriptor = field.getDescriptor();
        int slotId = field.getSlotId();
        OperandStack operandStack = frame.getOperandStack();
        switch (descriptor.charAt(0)) {
            case 'Z':
            case 'B':
            case 'C':
            case 'S':
            case 'I':
                int i = operandStack.popInt();
                getSlots(operandStack).setInt(slotId, i);
                break;
            case 'F':
                float f = operandStack.popFloat();
                getSlots(operandStack).setFloat(slotId, f);
                break;
            case 'J':
                long l = operandStack.popLong();
                getSlots(operandStack).setLong(slotId, l);
                break;
            case 'D':
                double d = operandStack.popDouble();
                getSlots(operandStack).setDouble(slotId, d);
                break;
            case 'L':
            case '[':
                Object o = operandStack.popRef();
                getSlots(operandStack).setRef(slotId, o);
                break;
            default:
                throw new RuntimeException("unknow type");
        }
    }

    private Slots getSlots(OperandStack operandStack) {
        Object object = operandStack.popRef();
        return object.getFields();
    }
}
