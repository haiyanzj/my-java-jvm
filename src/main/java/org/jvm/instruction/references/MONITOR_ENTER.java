package org.jvm.instruction.references;

import org.jvm.instruction.base.NoOperandsInstruction;
import org.jvm.rtda.Object;
import org.jvm.rtda.thread.Frame;

/**
 * @author 海燕
 * @date 2023/2/6
 */
public class MONITOR_ENTER extends NoOperandsInstruction {


    /**
     * @param frame
     */
    @Override
    public void execute(Frame frame) {
        Object ref = frame.getOperandStack().popRef();
        if (ref == null) {
            throw new NullPointerException();
        }
    }

}
