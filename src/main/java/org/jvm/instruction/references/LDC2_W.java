package org.jvm.instruction.references;

import org.jvm.instruction.base.Index16Instruction;
import org.jvm.rtda.thread.Frame;
import org.jvm.rtda.thread.OperandStack;

/**
 * 从常量池中获取常量值，压操作数栈
 *
 * @author 海燕
 * @date 2023/2/7
 */
public class LDC2_W extends Index16Instruction {

    @Override
    public void execute(Frame frame) {
        Object constant = frame.getMethod().getKlass().getConstantPool().getConstant(this.index);
        OperandStack operandStack = frame.getOperandStack();
        if (constant instanceof Long) {
            operandStack.pushLong((Long) constant);
        } else if (constant instanceof Double) {
            operandStack.pushDouble((Double) constant);
        } else {
            throw new RuntimeException("LDC2_W指令暂不支持其他情况");
        }
    }
}
