package org.jvm.instruction.references;

import org.jvm.instruction.base.Index16Instruction;
import org.jvm.instruction.base.InstructionUtil;
import org.jvm.rtda.Slots;
import org.jvm.rtda.heap.ConstantPool;
import org.jvm.rtda.heap.Klass;
import org.jvm.rtda.heap.classmember.Field;
import org.jvm.rtda.heap.symref.FieldRef;
import org.jvm.rtda.thread.Frame;
import org.jvm.rtda.thread.OperandStack;

/**
 * 从类的静态变量获取值，压操作数栈
 *
 * @author 海燕
 * @date 2023/2/6
 */
public class GET_STATIC extends Index16Instruction {


    /**
     * 指令操作数含义为类常量池索引
     *
     * @param frame
     */
    @Override
    public void execute(Frame frame) {
        //从代码执行类常量池获取一个字段符号引用
        Klass thisClass = frame.getMethod().getKlass();
        ConstantPool constantPool = thisClass.getConstantPool();
        FieldRef fieldRef = (FieldRef) constantPool.getConstant(this.index);
        //解析被取值字段符号引用
        Field field = fieldRef.resolvedField(frame.getThread());
        if (!field.isStatic()) {
            throw new RuntimeException("字段非静态");
        }
        //被取值字段所在的类
        Klass thatKlass = field.getKlass();
        //执行类初始化
        InstructionUtil.initClass(frame.getThread(), thatKlass);
        //给类静态变量根据不同类型进行赋值,从操作数栈中pop变量，赋值给类静态变量
        String descriptor = field.getDescriptor();
        int slotId = field.getSlotId();
        Slots staticVars = thatKlass.getStaticVars();
        OperandStack operandStack = frame.getOperandStack();
        switch (descriptor.charAt(0)) {
            case 'Z':
            case 'B':
            case 'C':
            case 'S':
            case 'I':
                operandStack.pushInt(staticVars.getInt(slotId));
                break;
            case 'F':
                operandStack.pushFloat(staticVars.getFloat(slotId));
                break;
            case 'J':
                operandStack.pushLong(staticVars.getLong(slotId));
                break;
            case 'D':
                operandStack.pushDouble(staticVars.getDouble(slotId));
                break;
            case 'L':
            case '[':
                operandStack.pushRef(staticVars.getRef(slotId));
                break;
            default:
                throw new RuntimeException("unknow type");
        }

    }
}
