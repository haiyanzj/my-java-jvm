package org.jvm.interpreter;

import org.jvm.JVM;
import org.jvm.instruction.base.ByteCodeReader;
import org.jvm.instruction.base.Instruction;
import org.jvm.instruction.base.InstructionFactory;
import org.jvm.rtda.thread.Frame;
import org.jvm.rtda.thread.Thread;

/**
 * 字节码解释器
 *
 * @author 海燕
 * @date 2023/1/29
 */
public class Interpret {

    /**
     * 线程解释器
     * 注意：线程栈并不一定从main或者Thread.run开始。起始栈高不一定为1。
     * 从虚拟机中直接发起JavaCall（例如隐式调用AppClassLoader），此时线程栈中可能有若干已经入栈的栈帧
     *
     * @param thread
     */
    public static void interpret(Thread thread) {
        //起始栈高
        int startStackSize = thread.getStack().getStack().size();
        ByteCodeReader reader = new ByteCodeReader();
        for (; thread.getStack().getStack().size() >= startStackSize; ) {
            //获取当前栈顶栈帧
            Frame frame = thread.getStack().top();
            int pc = frame.getNextPC();
            //获取对应的指令
            reader.reset(frame.getMethod().getCode(), pc);
            int opcode = reader.readUint8();
            Instruction instruction = InstructionFactory.newInstruction(opcode);
            if (JVM.log) {
                System.out.println(frame);
                System.out.println("  instruction:" + instruction.getClass().getSimpleName());
            }
            instruction.fetchOperands(reader);
            //指令执行。如果是分支指令会再改变帧中程序计数器的位置
            instruction.execute(frame);
            //如果栈帧程序计数器没有变化，那么执行本次的不是分支指令，直接顺序执行下一条指令
            if (pc == frame.getNextPC()) {
                frame.setNextPC(reader.getPC());
            }
        }
    }
}
