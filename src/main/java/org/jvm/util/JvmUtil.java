package org.jvm.util;

import org.jvm.rtda.Object;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.HashMap;
import java.util.Map;

/**
 * author : wangsixiang02
 * date : 2023/1/7
 * desc :
 */
public class JvmUtil {

    public static Map<String, String> primitiveTypes = new HashMap<String, String>() {
        {
            put("void", "V");
            put("boolean", "Z");
            put("byte", "B");
            put("short", "S");
            put("int", "I");
            put("long", "J");
            put("char", "C");
            put("float", "F");
            put("double", "D");
        }
    };

    public static byte[] readInputStream(InputStream inputStream) throws IOException {
        byte[] buffer = new byte[1024];
        int len = 0;
        ByteArrayOutputStream bos = new ByteArrayOutputStream();
        while ((len = inputStream.read(buffer)) != -1) {
            bos.write(buffer, 0, len);
        }
        bos.close();
        return bos.toByteArray();
    }

    /**
     * 将java的String实例转换为jvm中使用的字符串
     *
     * @param javaString
     * @return
     */
    public static String javaStringToJvmString(Object javaString) {
        if (javaString == null) {
            return null;
        }
        Object charArrayObject = javaString.getRefVar("value", "[C");
        return new String(charArrayObject.chars());
    }

    public static String toDescriptor(String className) {
        if (className.charAt(0) == '[') {
            // array
            return className;
        }
        if (primitiveTypes.containsKey(className)) {
            // primitive
            return primitiveTypes.get(className);
        }
        // object
        return "L" + className + ";";
    }

    public static String toClassName(String descriptor) {
        if (descriptor.charAt(0) == '[') {
            // array
            return descriptor;
        }
        if (descriptor.charAt(0) == 'L') {
            // object
            return descriptor.substring(1, descriptor.length() - 1);
        }
        for (Map.Entry<String, String> entry : primitiveTypes.entrySet()) {
            if (entry.getValue().equals(descriptor)) {
                return entry.getKey();
            }
        }
        throw new RuntimeException("unkonw descriptor");
    }


    /**
     * 将基本类型进行包装
     *
     * @param val jvm的基本类型
     * @return java包装类
     */
    public static Object box(java.lang.Object val) {
        if (val instanceof Boolean) {

        }

        return null;
    }

    public static String getComponentClassName(String klassName) {
        String componentClassName;
        //去掉前缀的一个[
        String subName = klassName.substring(1);
        //如果前缀依旧是[，说明依旧是一个数组类
        if (subName.startsWith("[")) {
            componentClassName = subName;
        }
        //如果前缀是L，则去掉L和末尾的;就是类名
        else if (subName.startsWith("L")) {
            componentClassName = subName.substring(1, subName.length() - 1);
        }
        //剩下的就是基本类型，根据描述符寻找基本类型名
        else {
            componentClassName = JvmUtil.primitiveTypes.entrySet().stream()
                    .filter(entry -> entry.getValue().equals(subName))
                    .map(Map.Entry::getKey).findAny().orElse("");
        }
        return componentClassName;
    }

}
