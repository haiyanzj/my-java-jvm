package org.jvm.rtda;

/**
 * 栈信息
 *
 * @author 海燕
 * @date 2023/2/23
 */
public class StackTraceElement {

    /**
     * 源文件名
     */
    private String fileName;

    /**
     * 类名
     */
    private String className;

    /**
     * 方法名
     */
    private String methodName;

    /**
     * 源代码行号
     */
    private int lineNumber;

    @Override
    public String toString() {
        return new StringBuilder("at ")
                .append(className)
                .append(".")
                .append(methodName)
                .append("(")
                .append(fileName)
                .append(":")
                .append(lineNumber)
                .append(")").toString();
    }

    public void setFileName(String fileName) {
        this.fileName = fileName;
    }

    public String getClassName() {
        return className;
    }

    public void setClassName(String className) {
        this.className = className;
    }

    public void setMethodName(String methodName) {
        this.methodName = methodName;
    }

    public void setLineNumber(int lineNumber) {
        this.lineNumber = lineNumber;
    }
}
