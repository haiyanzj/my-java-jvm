package org.jvm.rtda.heap.symref;

import org.jvm.rtda.heap.ConstantPool;

/**
 * 类引用
 *
 * @author 王思翔
 * @date 2023/2/4
 */
public class ClassRef extends SymRef {
    public ClassRef(ConstantPool cp, String className) {
        super(cp, className);
    }
}
