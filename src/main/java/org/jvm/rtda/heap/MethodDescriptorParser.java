package org.jvm.rtda.heap;

/**
 * 方法描述解析器
 *
 * @author 海燕
 * @date 2023/2/11
 */
public class MethodDescriptorParser {

    private String raw;

    private int offset;

    private MethodDescriptor parsed;

    public static MethodDescriptor parseMethodDescriptor(String descriptor) {
        MethodDescriptorParser parser = new MethodDescriptorParser();
        return parser.parse(descriptor);
    }

    private MethodDescriptor parse(String descriptor) {
        this.raw = descriptor;
        this.parsed = new MethodDescriptor();
        this.startParams();
        this.parseParamTypes();
        this.endParams();
        this.parseReturnType();
        this.finish();
        return this.parsed;
    }

    private void finish() {
        if (this.offset != this.raw.length()) {
            throw new RuntimeException("bad descriptor");
        }
    }

    private void parseReturnType() {
        if (this.readChar() == 'V') {
            this.parsed.setReturnType("V");
            return;
        }
        this.unreadChar();
        String t = this.parseFieldType();
        if (!t.equals("")) {
            this.parsed.setReturnType(t);
            return;
        }
        throw new RuntimeException("bad descriptor");
    }

    private void endParams() {
        if (readChar() != ')') {
            throw new RuntimeException("bad descriptor");
        }
    }

    private void parseParamTypes() {
        for (; ; ) {
            String t = parseFieldType();
            if (t != "") {
                this.parsed.addParameterType(t);
            } else {
                break;
            }
        }
    }

    private String parseFieldType() {
        switch (readChar()) {
            case 'B':
                return "B";
            case 'C':
                return "C";
            case 'D':
                return "D";
            case 'F':
                return "F";
            case 'I':
                return "I";
            case 'J':
                return "J";
            case 'S':
                return "S";
            case 'Z':
                return "Z";
            case 'L':
                return this.parseObjectType();
            case '[':
                return this.parseArrayType();
            default:
                this.unreadChar();
                return "";
        }
    }

    private String parseArrayType() {
        int arrStart = this.offset - 1;
        parseFieldType();
        int arrEnd = this.offset;
        return this.raw.substring(arrStart, arrEnd);
    }

    private String parseObjectType() {
        String unread = this.raw.substring(this.offset);
        int semicolonIndex = unread.indexOf(";");
        if (semicolonIndex == -1) {
            throw new RuntimeException("bad descriptor");
        } else {
            int objStart = this.offset - 1;
            int objEnd = this.offset + semicolonIndex + 1;
            this.offset = objEnd;
            String descriptor = this.raw.substring(objStart, objEnd);
            return descriptor;
        }
    }

    private void startParams() {
        if (readChar() != '(') {
            throw new RuntimeException("bad descriptor");
        }
    }

    private char readChar() {
        return this.raw.charAt(this.offset++);
    }

    private void unreadChar() {
        this.offset--;
    }

}
