package org.jvm.rtda.heap.classLoader;

import org.jvm.classpath.BootClassPath;
import org.jvm.rtda.Object;
import org.jvm.rtda.heap.Klass;
import org.jvm.rtda.thread.Thread;
import org.jvm.util.JvmUtil;

/**
 * JVM根类加载器，由JVM直接实现，不使用任何JavaCall
 *
 * @author 海燕
 * @date 2023/3/23
 */
public class BootKlassLoader extends AbstractKlassLoader {

    private BootClassPath classPath;

    public BootKlassLoader(BootClassPath classPath) {
        //根类加载器没有对应的java类加载器实例
        super(null);
        this.classPath = classPath;
    }

    public void init() {
        //先加载基本类的类对象
        loadBasicClass();
        //再加载基本类型的类
        loadPrimitiveClasses();
    }

    public Klass loadKlass(String klassName) {
        return loadKlass(klassName, null);
    }

    /**
     * 根类加载器并不使用javaCall。thread为空
     *
     * @param klassName
     * @param thread
     * @return
     */
    @Override
    public Klass doLoadNoArrayKlass(String klassName, Thread thread) {
        byte[] klassData = classPath.readClass(klassName);
        return defineNoArrayKlass(klassName, klassData, null);
    }

    /**
     * 类加载器初始化后，立刻对存量的类加载类对象
     */
    private void loadBasicClass() {
        loadKlass("java/lang/Class");
        /**
         * 加载java/lang/Class后，因为加载类需要先加载其父类。方法区中实际上有多个类
         */
        this.klassMap.values().forEach(item -> fillJavaClassAndJavaClassLoader(item));
    }

    /**
     * 基本类型也有对象的类，在这里进行加载
     */
    private void loadPrimitiveClasses() {
        JvmUtil.primitiveTypes.keySet().forEach(primitiveClassName -> {
            Klass primitiveClass = Klass.newPrimitiveClass(primitiveClassName, this);
            //基本类型的类不通过loadClass方法加载，需要手动放入方法区并加载类对象
            klassMap.put(primitiveClassName, primitiveClass);
            Klass classKlass = loadKlass("java/lang/Class");
            Object classObject = classKlass.newObject();
            classObject.setExtra(primitiveClass);
            primitiveClass.setjClass(classObject);
        });
    }

}
