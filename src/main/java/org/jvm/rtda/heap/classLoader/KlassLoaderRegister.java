package org.jvm.rtda.heap.classLoader;

import org.jvm.classpath.BootClassPath;
import org.jvm.rtda.Object;

/**
 * @author 海燕
 * @date 2023/3/23
 */
public class KlassLoaderRegister {

    /**
     * 根类加载器
     */
    private static BootKlassLoader bootKlassLoader;

    /**
     * 初始化根类加载器
     *
     * @param classPath
     */
    public static void initBootKlassLoader(BootClassPath classPath) {
        bootKlassLoader = new BootKlassLoader(classPath);
        bootKlassLoader.init();
    }

    /**
     * 根据java类加载器实例返回JVM中对应的java类加载器，如果没有则新建并注册
     *
     * @param javaClassLoaderInstance
     * @return
     */
    public static JavaKlassLoader getJavaKlassLoader(Object javaClassLoaderInstance) {
        JavaKlassLoader javaKlassLoader = (JavaKlassLoader) javaClassLoaderInstance.getExtra();
        if (javaKlassLoader != null) {
            return javaKlassLoader;
        }
        javaKlassLoader = new JavaKlassLoader(javaClassLoaderInstance);
        javaClassLoaderInstance.setExtra(javaKlassLoader);
        return javaKlassLoader;
    }

    public static BootKlassLoader getBootKlassLoader() {
        return bootKlassLoader;
    }

}
