package org.jvm.nativemethod.methods.java.lang;

import org.jvm.nativemethod.NativeMethodRegister;
import org.jvm.rtda.thread.Frame;

/**
 * @author 海燕
 * @date 2023/2/26
 */
public class PackageNativeMethod extends NativeMethodRegister {

    public static void init() throws NoSuchMethodException {
        register("java/lang/Package", "getSystemPackage0", "(Ljava/lang/String;)Ljava/lang/String;", PackageNativeMethod.class.getDeclaredMethod("getSystemPackage0", Frame.class));
    }

    public static void getSystemPackage0(Frame frame) {
        frame.getOperandStack().pushRef(null);
    }
}
