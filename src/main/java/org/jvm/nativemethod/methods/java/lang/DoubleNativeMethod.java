package org.jvm.nativemethod.methods.java.lang;

import org.jvm.nativemethod.NativeMethodRegister;
import org.jvm.rtda.thread.Frame;

/**
 * @author 海燕
 * @date 2023/2/26
 */
public class DoubleNativeMethod extends NativeMethodRegister {

    public static void init() throws NoSuchMethodException {
        register("java/lang/Double", "doubleToRawLongBits", "(D)J", DoubleNativeMethod.class.getDeclaredMethod("doubleToRawLongBits", Frame.class));
        register("java/lang/Double", "longBitsToDouble", "(J)D", DoubleNativeMethod.class.getDeclaredMethod("longBitsToDouble", Frame.class));
    }

    public static void doubleToRawLongBits(Frame frame) {
        double value = frame.getLocalVars().getDouble(0);
        frame.getOperandStack().pushLong(Double.doubleToRawLongBits(value));
    }

    public static void longBitsToDouble(Frame frame) {
        long value = frame.getLocalVars().getLong(0);
        frame.getOperandStack().pushDouble(Double.longBitsToDouble(value));
    }

}
