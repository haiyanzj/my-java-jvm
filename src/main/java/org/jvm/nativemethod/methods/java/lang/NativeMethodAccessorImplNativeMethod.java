package org.jvm.nativemethod.methods.java.lang;

import org.jvm.instruction.base.InstructionUtil;
import org.jvm.instruction.references.ATHROW;
import org.jvm.nativemethod.NativeMethodRegister;
import org.jvm.rtda.Object;
import org.jvm.rtda.heap.Klass;
import org.jvm.rtda.heap.classLoader.KlassLoaderRegister;
import org.jvm.rtda.heap.classmember.Method;
import org.jvm.rtda.heap.stringpool.StringPool;
import org.jvm.rtda.thread.Frame;
import org.jvm.rtda.thread.LocalVars;
import org.jvm.util.JavaCallUtil;
import org.jvm.util.JvmUtil;

/**
 * @author 海燕
 * @date 2024/11/3
 */
public class NativeMethodAccessorImplNativeMethod extends NativeMethodRegister {

    public static void init() throws NoSuchMethodException {
        register("sun/reflect/NativeMethodAccessorImpl", "invoke0",
                "(Ljava/lang/reflect/Method;Ljava/lang/Object;[Ljava/lang/Object;)Ljava/lang/Object;",
                NativeMethodAccessorImplNativeMethod.class.getDeclaredMethod("invoke0", Frame.class));

    }

    public static void invoke0(Frame frame) {
        LocalVars localVars = frame.getLocalVars();
        Object javaMethod = localVars.getRef(0);
        //实例对象，如果是实例方法有值，如果是static方法就是null
        Object obj = localVars.getRef(1);
        //方法参数列表
        Object[] args = (Object[]) localVars.getRef(2).getData();
        //先根据参数和返回值描述符初步找到要执行的方法
        //找到方法所在的类
        Object methodJavaClass = javaMethod.getRefVar("clazz", "Ljava/lang/Class;");
        Klass methodKlass = (Klass) methodJavaClass.getExtra();
        //初始化方法所在类
        InstructionUtil.initClass(frame.getThread(), methodKlass);
        //获取参数和返回值类型，构造方法描述符
        Object[] parameterTypes = (Object[]) javaMethod.getRefVar("parameterTypes", "[Ljava/lang/Class;").getData();
        Object returnType = javaMethod.getRefVar("returnType", "Ljava/lang/Class;");
        StringBuilder escriptorSb = new StringBuilder("(");
        for (Object parameterType : parameterTypes) {
            escriptorSb.append(JvmUtil.toDescriptor(((Klass) parameterType.getExtra()).getName()));
        }
        escriptorSb.append(")");
        escriptorSb.append(JvmUtil.toDescriptor(((Klass) returnType.getExtra()).getName()));
        String escriptor = escriptorSb.toString();
        //根据方法名和描述找到要执行的方法
        String methodName = JvmUtil.javaStringToJvmString(javaMethod.getRefVar("name", "Ljava/lang/String;"));
        Method jvmMethod = methodKlass.getStaticMethod(methodName, escriptor);
        if (jvmMethod == null) {
            jvmMethod = methodKlass.getInstanceMethod(methodName, escriptor);
        }
        if (jvmMethod == null) {
            throwIllegalArgumentException(frame);
            return;
        }

        //初步找到方法后，验证参数的实现类是否可以转换
        if (args.length != parameterTypes.length) {
            throwIllegalArgumentException(frame);
            return;
        }
        for (int i = 0; i < args.length; i++) {
            //传入参数的实例的实际实现类
            Klass argsInstanceKlass = args[i].getKlass();
            //被调用方法入参声明类型
            //注意，即使全类名一致，如果被不同类加载器加载，也是不同的类。
            Klass argsKlass = (Klass) parameterTypes[i].getExtra();
            if (!argsKlass.isAssignableFrom(argsInstanceKlass)) {
                throwIllegalArgumentException(frame);
                return;
            }
        }

        //执行方法
        Object result = (Object) JavaCallUtil.javaCall(frame.getThread(), jvmMethod, obj, args);
        //返回结果
        frame.getOperandStack().pushRef(result);
    }

    private static void throwIllegalArgumentException(Frame frame) {
        Klass iaeKlass = KlassLoaderRegister.getBootKlassLoader().loadKlass("java/lang/IllegalArgumentException");
        Object iaeRef = iaeKlass.newObject();
        JavaCallUtil.javaCall(frame.getThread(),
                iaeKlass.getInstanceMethod("<init>", "(Ljava/lang/String;)V"),
                iaeRef,
                StringPool.jString("argument type mismatch"));
        ATHROW.throwException(KlassLoaderRegister.getBootKlassLoader(), iaeRef, frame.getThread());
    }

}
