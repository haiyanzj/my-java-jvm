package org.jvm.nativemethod.methods.java.util;

import org.jvm.nativemethod.NativeMethodRegister;
import org.jvm.rtda.thread.Frame;

/**
 * @author 海燕
 * @date 2023/2/26
 */
public class JarFileNativeMethod extends NativeMethodRegister {

    public static void init() throws NoSuchMethodException {
        register("java/util/jar/JarFile", "getMetaInfEntryNames", "()[Ljava/lang/String;", JarFileNativeMethod.class.getDeclaredMethod("getMetaInfEntryNames", Frame.class));
    }

    public static void getMetaInfEntryNames(Frame frame) {
        frame.getOperandStack().pushRef(null);
    }

}
