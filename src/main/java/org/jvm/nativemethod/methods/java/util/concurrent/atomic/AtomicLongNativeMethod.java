package org.jvm.nativemethod.methods.java.util.concurrent.atomic;

import org.jvm.nativemethod.NativeMethodRegister;
import org.jvm.rtda.thread.Frame;

/**
 * @author 海燕
 * @date 2023/2/26
 */
public class AtomicLongNativeMethod extends NativeMethodRegister {

    public static void init() throws NoSuchMethodException {
        register("java/util/concurrent/atomic/AtomicLong", "VMSupportsCS8", "()Z", AtomicLongNativeMethod.class.getDeclaredMethod("VMSupportsCS8", Frame.class));
    }

    public static void VMSupportsCS8(Frame frame) {
        frame.getOperandStack().pushBoolean(false);
    }

}
