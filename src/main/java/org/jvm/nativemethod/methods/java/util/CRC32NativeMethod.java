package org.jvm.nativemethod.methods.java.util;

import org.jvm.nativemethod.NativeMethodRegister;
import org.jvm.rtda.thread.Frame;
import org.jvm.rtda.thread.LocalVars;

import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.util.zip.CRC32;

/**
 * @author 海燕
 * @date 2024/12/10
 */
public class CRC32NativeMethod extends NativeMethodRegister {

    public static void init() throws NoSuchMethodException {
        register("java/util/zip/CRC32", "updateBytes", "(I[BII)I", CRC32NativeMethod.class.getDeclaredMethod("updateBytes", Frame.class));
    }

    //    private native static int updateBytes(int crc, byte[] b, int off, int len);
    public static void updateBytes(Frame frame) throws NoSuchMethodException, InvocationTargetException, IllegalAccessException {
        LocalVars localVars = frame.getLocalVars();
        int crc = localVars.getInt(0);
        byte[] b = (byte[]) localVars.getRef(1).getData();
        int off = localVars.getInt(2);
        int len = localVars.getInt(3);
        Method method = CRC32.class.getDeclaredMethod("updateBytes", int.class, byte[].class, int.class, int.class);
        method.setAccessible(true);
        int result = (int) method.invoke(null, crc, b, off, len);
        frame.getOperandStack().pushInt(result);
    }

}
