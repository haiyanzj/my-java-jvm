package org.jvm.nativemethod.methods.java.lang;

import org.jvm.nativemethod.NativeMethodRegister;
import org.jvm.rtda.Object;
import org.jvm.rtda.heap.stringpool.StringPool;
import org.jvm.rtda.thread.Frame;

/**
 * @author 海燕
 * @date 2023/2/26
 */
public class StringNativeMethod extends NativeMethodRegister {

    public static void init() throws NoSuchMethodException {
        register("java/lang/String", "intern", "()Ljava/lang/String;", StringNativeMethod.class.getDeclaredMethod("intern", Frame.class));

    }

    /**
     * java/lang/String 中本地方法
     * 将字符串放入字符串池。如果已经入池则返回池中对象
     *
     * @param frame
     */
    public static void intern(Frame frame) {
        Object javaString = frame.getLocalVars().getRef(0);
        Object interString = StringPool.inter(javaString);
        frame.getOperandStack().pushRef(interString);
    }

}
