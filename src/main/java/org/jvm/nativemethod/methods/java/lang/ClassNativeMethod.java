package org.jvm.nativemethod.methods.java.lang;

import org.jvm.instruction.base.InstructionUtil;
import org.jvm.nativemethod.NativeMethodRegister;
import org.jvm.rtda.Object;
import org.jvm.rtda.heap.Klass;
import org.jvm.rtda.heap.classLoader.AbstractKlassLoader;
import org.jvm.rtda.heap.classLoader.KlassLoaderRegister;
import org.jvm.rtda.heap.classmember.Field;
import org.jvm.rtda.heap.classmember.Method;
import org.jvm.rtda.heap.stringpool.StringPool;
import org.jvm.rtda.thread.Frame;
import org.jvm.rtda.thread.LocalVars;
import org.jvm.rtda.thread.OperandStack;
import org.jvm.rtda.thread.Thread;
import org.jvm.util.JavaCallUtil;
import org.jvm.util.JvmUtil;

import java.util.Arrays;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

/**
 * @author 海燕
 * @date 2023/2/26
 */
public class ClassNativeMethod extends NativeMethodRegister {

    private static String jlClass = "java/lang/Class";

    public static void init() throws NoSuchMethodException {
        register(jlClass, "getPrimitiveClass", "(Ljava/lang/String;)Ljava/lang/Class;", ClassNativeMethod.class.getDeclaredMethod("getPrimitiveClass", Frame.class));
        register(jlClass, "getName0", "()Ljava/lang/String;", ClassNativeMethod.class.getDeclaredMethod("getName0", Frame.class));
        register(jlClass, "desiredAssertionStatus0", "(Ljava/lang/Class;)Z", ClassNativeMethod.class.getDeclaredMethod("desiredAssertionStatus0", Frame.class));
        register(jlClass, "isAssignableFrom", "(Ljava/lang/Class;)Z", ClassNativeMethod.class.getDeclaredMethod("isAssignableFrom", Frame.class));
        register(jlClass, "isInterface", "()Z", ClassNativeMethod.class.getDeclaredMethod("isInterface", Frame.class));
        register(jlClass, "isPrimitive", "()Z", ClassNativeMethod.class.getDeclaredMethod("isPrimitive", Frame.class));
        register(jlClass, "getDeclaredFields0", "(Z)[Ljava/lang/reflect/Field;", ClassNativeMethod.class.getDeclaredMethod("getDeclaredFields0", Frame.class));
        register(jlClass, "forName0", "(Ljava/lang/String;ZLjava/lang/ClassLoader;Ljava/lang/Class;)Ljava/lang/Class;", ClassNativeMethod.class.getDeclaredMethod("forName0", Frame.class));
        register(jlClass, "getDeclaredConstructors0", "(Z)[Ljava/lang/reflect/Constructor;", ClassNativeMethod.class.getDeclaredMethod("getDeclaredConstructors0", Frame.class));
        register(jlClass, "getModifiers", "()I", ClassNativeMethod.class.getDeclaredMethod("getModifiers", Frame.class));
        register(jlClass, "getSuperclass", "()Ljava/lang/Class;", ClassNativeMethod.class.getDeclaredMethod("getSuperclass", Frame.class));
        register(jlClass, "getInterfaces0", "()[Ljava/lang/Class;", ClassNativeMethod.class.getDeclaredMethod("getInterfaces0", Frame.class));
        register(jlClass, "isArray", "()Z", ClassNativeMethod.class.getDeclaredMethod("isArray", Frame.class));
        register(jlClass, "getDeclaredMethods0", "(Z)[Ljava/lang/reflect/Method;", ClassNativeMethod.class.getDeclaredMethod("getDeclaredMethods0", Frame.class));
        register(jlClass, "getComponentType", "()Ljava/lang/Class;", ClassNativeMethod.class.getDeclaredMethod("getComponentType", Frame.class));
    }

    /**
     * java/lang/Class获取基本类型类的方法，入参是一个String为基本类名
     *
     * @param frame
     */
    public static void getPrimitiveClass(Frame frame) {
        Object classNameString = frame.getLocalVars().getRef(0);
        String primitiveClassName = JvmUtil.javaStringToJvmString(classNameString);
        Klass primitiveClass = KlassLoaderRegister.getBootKlassLoader().loadKlass(primitiveClassName);
        Object primitiveClassObject = primitiveClass.getjClass();
        frame.getOperandStack().pushRef(primitiveClassObject);
    }

    /**
     * java/lang/Class中方法，获取类名
     *
     * @param frame
     */
    public static void getName0(Frame frame) {
        //拿到类对象的引用
        Object thisRef = frame.getLocalVars().getRef(0);
        //获取类对象指向的类
        Klass refClass = (Klass) thisRef.getExtra();
        String javaName = refClass.getJavaName();
        //TODO 本地方法返回的String会被放入字符串池？这个结论确定吗？
        Object javaString = StringPool.jString(javaName);
        frame.getOperandStack().pushRef(javaString);
    }

    /**
     * 不讨论断言，默认给false
     *
     * @param frame
     */
    public static void desiredAssertionStatus0(Frame frame) {
        frame.getOperandStack().pushInt(0);
    }

    public static void isInterface(Frame frame) {
        Klass klass = (Klass) frame.getLocalVars().getRef(0).getExtra();
        frame.getOperandStack().pushBoolean(klass.isInterface());
    }

    public static void isPrimitive(Frame frame) {
        Klass klass = (Klass) frame.getLocalVars().getRef(0).getExtra();
        frame.getOperandStack().pushBoolean(klass.isPrimitive());
    }

    /**
     * 通过类名加载类
     *
     * @param frame
     */
    public static void forName0(Frame frame) {
        LocalVars localVars = frame.getLocalVars();
        Object javaClassNameRef = localVars.getRef(0);
        boolean initialize = localVars.getBoolean(1);
        Object jLoader = localVars.getRef(2);
        String javaClassName = JvmUtil.javaStringToJvmString(javaClassNameRef).replace('.', '/');
        AbstractKlassLoader klassLoader = (AbstractKlassLoader) Optional.ofNullable(jLoader).map(Object::getExtra).orElse(KlassLoaderRegister.getBootKlassLoader());
        Klass jvmClass = klassLoader.loadKlass(javaClassName, frame.getThread());
        Object javaClass = jvmClass.getjClass();
        //如果需要初始化的，需要先初始化该类
        if (initialize) {
            //执行类初始化
            InstructionUtil.initClass(frame.getThread(), jvmClass);
        }
        frame.getOperandStack().pushRef(javaClass);
    }

    /**
     * 获取类修饰符
     *
     * @param frame
     */
    public static void getModifiers(Frame frame) {
        LocalVars localVars = frame.getLocalVars();
        Object thisRef = localVars.getThis();
        Klass klass = (Klass) thisRef.getExtra();
        int modifiers = klass.getAccessFlags();
        frame.getOperandStack().pushInt(modifiers);
    }

    public static void getSuperclass(Frame frame) {
        LocalVars localVars = frame.getLocalVars();
        Object thisRef = localVars.getThis();
        Klass klass = (Klass) thisRef.getExtra();
        Klass superClass = klass.getSuperClass();
        Object res = Optional.ofNullable(superClass).map(Klass::getjClass).orElse(null);
        frame.getOperandStack().pushRef(res);
    }

    /**
     * 返回类所实现的接口数组
     *
     * @param frame
     */
    public static void getInterfaces0(Frame frame) {
        LocalVars localVars = frame.getLocalVars();
        Object thisRef = localVars.getThis();
        Klass klass = (Klass) thisRef.getExtra();
        Klass[] interfaces = klass.getInterfaces();
        Object classArray = toClassArr(interfaces);
        frame.getOperandStack().pushRef(classArray);
    }

    private static Object toClassArr(Klass[] interfaces) {
        Klass arrayClass = KlassLoaderRegister.getBootKlassLoader().loadKlass("[Ljava/lang/Class;");
        Object array = arrayClass.newArray(interfaces.length);
        Object[] refs = array.refs();
        for (int i = 0; i < interfaces.length; i++) {
            refs[i] = interfaces[i].getjClass();
        }
        return array;
    }

    public static void isArray(Frame frame) {
        LocalVars localVars = frame.getLocalVars();
        Object thisRef = localVars.getThis();
        Klass klass = (Klass) thisRef.getExtra();
        frame.getOperandStack().pushBoolean(klass.isArray());
    }

    /**
     * 获取数组类的元素类
     *
     * @param frame
     */
    public static void getComponentType(Frame frame) {
        LocalVars localVars = frame.getLocalVars();
        Object thisRef = localVars.getThis();
        Klass klass = (Klass) thisRef.getExtra();
        Klass componentClass = klass.getComponentClass();
        frame.getOperandStack().pushRef(componentClass.getjClass());
    }

    public static void isAssignableFrom(Frame frame) {
        LocalVars localVars = frame.getLocalVars();
        Klass thisClass = (Klass) localVars.getThis().getExtra();
        Klass thatClass = (Klass) localVars.getRef(1).getExtra();
        frame.getOperandStack().pushBoolean(thisClass.isAssignableFrom(thatClass));
    }

    private static final String _constructorConstructorDescriptor = "" +
            "(Ljava/lang/Class;" +
            "[Ljava/lang/Class;" +
            "[Ljava/lang/Class;" +
            "II" +
            "Ljava/lang/String;" +
            "[B[B)V";

    public static void getDeclaredConstructors0(Frame frame) {
        LocalVars vars = frame.getLocalVars();
        Object classObj = vars.getThis();
        boolean publicOnly = vars.getBoolean(1);
        Klass klass = (Klass) classObj.getExtra();
        Method[] constructors = klass.getConstructors(publicOnly);
        int constructorCount = constructors.length;
        Klass constructorClass = KlassLoaderRegister.getBootKlassLoader().loadKlass("java/lang/reflect/Constructor");
        Object fieldArr = constructorClass.arrayClass(frame.getThread()).newArray(constructorCount);
        OperandStack stack = frame.getOperandStack();
        stack.pushRef(fieldArr);
        if (constructorCount <= 0) {
            return;
        }
        Thread thread = frame.getThread();
        Object[] constructorObjs = fieldArr.refs();
        //获取java/lang/reflect/Constructor的构造器
        Method constructorInitMethod = constructorClass.getConstructor(_constructorConstructorDescriptor);

        for (int i = 0; i < constructors.length; i++) {
            Method constructor = constructors[i];
            //对于java/lang/reflect/Constructor的实例来说，extra字段指向构造器方法
            Object constructorObj = constructorClass.newObject();
            constructorObj.setExtra(constructor);
            constructorObjs[i] = constructorObj;
            //借用java构造器进行初始化
            JavaCallUtil.javaCall(thread, constructorInitMethod, constructorObj,
                    classObj,
                    toClassArr(constructor.getParameterTypes(klass.getKlassLoader(), thread)),
                    toClassArr(constructor.getExceptionTypes(thread)),
                    constructor.getAccessFlags(),
                    0,
                    getSignatureStr(constructor.signature()),
                    toByteArr(constructor.getAnnotationData()),
                    toByteArr(constructor.getParameterAnnotationData())
            );
        }
    }

    private static Object toByteArr(byte[] annotationData) {
        Klass klass = KlassLoaderRegister.getBootKlassLoader().loadKlass("[B");
        Object byteArray = klass.newArray(annotationData.length);
        byteArray.setData(annotationData);
        return byteArray;
    }

    private static Object getSignatureStr(String signature) {
        return null;
    }

    final static String _fieldConstructorDescriptor = "" +
            "(Ljava/lang/Class;" +
            "Ljava/lang/String;" +
            "Ljava/lang/Class;" +
            "II" +
            "Ljava/lang/String;" +
            "[B)V";

    public static void getDeclaredFields0(Frame frame) {
        LocalVars vars = frame.getLocalVars();
        Object classObj = vars.getThis();
        boolean publicOnly = vars.getBoolean(1);

        Klass klass = (Klass) classObj.getExtra();
        Field[] fields = klass.getFields(publicOnly);
        int fieldCount = fields.length;

        Klass fieldClass = KlassLoaderRegister.getBootKlassLoader().loadKlass("java/lang/reflect/Field");
        Object fieldArr = fieldClass.arrayClass(frame.getThread()).newArray(fieldCount);

        OperandStack stack = frame.getOperandStack();
        stack.pushRef(fieldArr);

        if (fieldCount <= 0) {
            return;
        }
        Thread thread = frame.getThread();
        Object[] fieldObjs = fieldArr.refs();
        //获取java/lang/reflect/Constructor的构造器
        Method constructorInitMethod = fieldClass.getConstructor(_fieldConstructorDescriptor);
        for (int i = 0; i < fields.length; i++) {
            Field field = fields[i];
            //对于java/lang/reflect/Constructor的实例来说，extra字段指向构造器方法
            Object jField = fieldClass.newObject();
            jField.setExtra(field);
            fieldObjs[i] = jField;
            //借用java构造器进行初始化
            JavaCallUtil.javaCall(thread, constructorInitMethod, jField,
                    classObj, StringPool.jString(field.getName()),
                    field.getType(klass.getKlassLoader(), thread).getjClass(),
                    field.getAccessFlags(),
                    field.getSlotId(),
                    getSignatureStr(field.signature()),
                    toByteArr(field.getAnnotationData())
            );
        }
    }

    final static String _methodConstructorDescriptor = "" +
            "(Ljava/lang/Class;" +
            "Ljava/lang/String;" +
            "[Ljava/lang/Class;" +
            "Ljava/lang/Class;" +
            "[Ljava/lang/Class;" +
            "II" +
            "Ljava/lang/String;" +
            "[B[B[B)V";


    public static void getDeclaredMethods0(Frame frame) {
        LocalVars vars = frame.getLocalVars();
        Object classObj = vars.getThis();
        boolean publicOnly = vars.getBoolean(1);
        Klass klass = (Klass) classObj.getExtra();
        //过滤构造器和类初始化方法
        List<Method> methods = Arrays.stream(klass.getMethods(publicOnly))
                .filter(method -> !method.getName().equals("<init>"))
                .filter(method -> !method.getName().equals("<clinit>"))
                .collect(Collectors.toList());
        int methodCount = methods.size();
        Klass methodClass = KlassLoaderRegister.getBootKlassLoader().loadKlass("java/lang/reflect/Method");
        Object methodArr = methodClass.arrayClass(frame.getThread()).newArray(methodCount);

        OperandStack stack = frame.getOperandStack();
        stack.pushRef(methodArr);
        if (methodCount <= 0) {
            return;
        }
        Thread thread = frame.getThread();
        Object[] methodObjs = methodArr.refs();
        //获取java/lang/reflect/Constructor的构造器
        Method methodConstructor = methodClass.getConstructor(_methodConstructorDescriptor);

        for (int i = 0; i < methods.size(); i++) {
            Method method = methods.get(i);
            //对于java/lang/reflect/Constructor的实例来说，extra字段指向构造器方法
            Object methodObj = methodClass.newObject();
            methodObj.setExtra(method);
            methodObjs[i] = methodObj;
            JavaCallUtil.javaCall(frame.getThread(), methodConstructor,
                    methodObj,
                    classObj,
                    StringPool.jString(method.getName()),
                    toClassArr(method.getParameterTypes(klass.getKlassLoader(), thread)),
                    method.getReturnType(klass.getKlassLoader(), thread).getjClass(),
                    toClassArr(method.getExceptionTypes(thread)),
                    method.getAccessFlags(),
                    0,
                    getSignatureStr(method.signature()),
                    toByteArr(method.getAnnotationData()),
                    toByteArr(method.getParameterAnnotationData()),
                    toByteArr(method.getAnnotationDefaultData())
            );
        }
    }

}
