package org.jvm.nativemethod.methods.sun.misc;

import org.jvm.instruction.base.InstructionUtil;
import org.jvm.nativemethod.NativeMethodRegister;
import org.jvm.rtda.heap.Klass;
import org.jvm.rtda.heap.classLoader.KlassLoaderRegister;
import org.jvm.rtda.heap.classmember.Method;
import org.jvm.rtda.thread.Frame;

/**
 * @author 海燕
 * @date 2023/2/26
 */
public class VMNativeMethod extends NativeMethodRegister {

    public static void init() throws NoSuchMethodException {
        register("sun/misc/VM", "initialize", "()V", VMNativeMethod.class.getDeclaredMethod("initialize", Frame.class));
    }


    public static void initialize(Frame frame) {
        Klass jlSysClass = KlassLoaderRegister.getBootKlassLoader().loadKlass("java/lang/System");
        Method initSysClass = jlSysClass.getStaticMethod("initializeSystemClass", "()V");
        InstructionUtil.invokeMethod(frame, initSysClass);
    }

}
