package org.jvm.nativemethod.methods.sun.misc;

import org.jvm.instruction.base.InstructionUtil;
import org.jvm.nativemethod.NativeMethodRegister;
import org.jvm.rtda.heap.Klass;
import org.jvm.rtda.heap.classLoader.KlassLoaderRegister;
import org.jvm.rtda.heap.classmember.Method;
import org.jvm.rtda.thread.Frame;

/**
 * @author 海燕
 * @date 2023/2/26
 */
public class PerfNativeMethod extends NativeMethodRegister {

    public static void init() throws NoSuchMethodException {
        register("sun/misc/Perf", "createLong", "(Ljava/lang/String;IIJ)Ljava/nio/ByteBuffer;", PerfNativeMethod.class.getDeclaredMethod("createLong", Frame.class));
    }


    public static void createLong(Frame frame) {
        Klass bbClass = KlassLoaderRegister.getBootKlassLoader().loadKlass("java/nio/ByteBuffer");
        //执行类初始化
        InstructionUtil.initClass(frame.getThread(), bbClass);
        frame.getOperandStack().pushInt(8);
        Method allocate = bbClass.getStaticMethod("allocate", "(I)Ljava/nio/ByteBuffer;");
        InstructionUtil.invokeMethod(frame, allocate);
    }

}
