package org.jvm.nativemethod;

import org.jvm.rtda.heap.Klass;
import org.jvm.rtda.heap.classmember.Method;
import org.jvm.rtda.thread.Frame;
import org.jvm.rtda.thread.OperandStack;
import org.jvm.rtda.thread.Thread;

/**
 * @author 王思翔
 * @date 2023/3/4
 */
public class NativeMethodUtil {

    private static Method shimMethod;

    public static Klass shimClass;

    static {
        shimMethod = new Method();
        shimMethod.setCode(new byte[]{(byte) 0xb1});
        shimClass = new Klass();
        shimClass.setName("shim");
        shimMethod.setName("shim");
        shimMethod.setKlass(shimClass);
    }

    /**
     * 获取一个间隙栈帧，承接栈顶的return。本身不执行任何操作
     *
     * @param thread
     * @param stack
     * @return
     */
    public static Frame newShimFrame(Thread thread, OperandStack stack) {
        Frame shimFrame = thread.newFrameAndPush(shimMethod);
        shimFrame.setOperandStack(stack);
        return shimFrame;
    }
}
