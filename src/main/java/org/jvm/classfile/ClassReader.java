package org.jvm.classfile;

import org.jvm.util.ByteArrayReader;

/**
 * author : wangsixiang02
 * date : 2023/1/7
 * desc :读取.class二进制文件
 */
public class ClassReader extends ByteArrayReader {

    public ClassReader(byte[] data) {
        super(data);
    }
}