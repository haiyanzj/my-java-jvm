package org.jvm.classfile.constantinfo;

import org.jvm.classfile.ClassReader;

/**
 * author : wangsixiang02
 * date : 2023/1/7
 * desc :.class文件单个常量信息
 */
public interface ConstantInfo {

    void readInfo(ClassReader classReader);

}
