package org.jvm.classfile.constantinfo;

import org.jvm.classfile.ClassReader;

/**
 * author : wangsixiang02
 * date : 2023/1/7
 * desc :
 */
public class ConstantDoubleInfo implements ConstantInfo {

    private double val;

    public double getVal() {
        return val;
    }

    @Override
    public void readInfo(ClassReader classReader) {
        this.val = Double.longBitsToDouble(classReader.readUint64());
    }
}
