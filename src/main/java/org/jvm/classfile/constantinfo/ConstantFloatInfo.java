package org.jvm.classfile.constantinfo;

import org.jvm.classfile.ClassReader;

/**
 * author : wangsixiang02
 * date : 2023/1/7
 * desc :
 */
public class ConstantFloatInfo implements ConstantInfo {

    private float val;

    public float getVal() {
        return val;
    }

    @Override
    public void readInfo(ClassReader classReader) {
        this.val = Float.intBitsToFloat(classReader.readUint32());
    }
}
