package org.jvm.classfile.constantinfo;

import org.jvm.classfile.ClassReader;
import org.jvm.classfile.ConstantPool;

/**
 * author : wangsixiang02
 * date : 2023/1/7
 * desc :
 */
public class ConstantStringInfo implements ConstantInfo {

    private final ConstantPool constantPool;

    /**
     * 指向常量池的一个Utf-8字符组常量的索引
     */
    private int stringIndex;

    public ConstantStringInfo(ConstantPool constantPool) {
        this.constantPool = constantPool;
    }

    @Override
    public void readInfo(ClassReader classReader) {
        this.stringIndex = classReader.readUint16();
    }

    /**
     * 根据索引寻址，返回对应字符串
     *
     * @return
     */
    public String string() {
        return constantPool.getUtf8(this.stringIndex);
    }
}
