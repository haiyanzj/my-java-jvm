package org.jvm.classfile.constantinfo;

import org.jvm.classfile.ClassReader;
import org.jvm.classfile.ConstantPool;

/**
 * author : wangsixiang02
 * date : 2023/1/7
 * desc :本类中引用的其他类成员标识
 */
public class ConstantMemberrefInfo implements ConstantInfo {

    private final ConstantPool constantPool;

    private int classIndex;

    private int nameAndTypeIndex;

    public ConstantMemberrefInfo(ConstantPool constantPool) {
        this.constantPool = constantPool;
    }

    @Override
    public void readInfo(ClassReader classReader) {
        this.classIndex = classReader.readUint16();
        this.nameAndTypeIndex = classReader.readUint16();
    }

    public String className() {
        ConstantClassInfo constantClassInfo = (ConstantClassInfo) this.constantPool.getConstantInfoByIndex(classIndex);
        return constantClassInfo.name();
    }

    public ConstantNameAndTypeInfo nameAndDescriptor() {
        ConstantNameAndTypeInfo constantNameAndTypeInfo = (ConstantNameAndTypeInfo) this.constantPool.getConstantInfoByIndex(nameAndTypeIndex);
        return constantNameAndTypeInfo;
    }
}
