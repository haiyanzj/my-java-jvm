package org.jvm.classfile.constantinfo;

import org.jvm.classfile.ClassReader;

/**
 * author : wangsixiang02
 * date : 2023/1/7
 * desc :
 */
public class ConstantLongInfo implements ConstantInfo {

    private long val;

    public long getVal() {
        return val;
    }

    @Override
    public void readInfo(ClassReader classReader) {
        this.val = classReader.readUint64();
    }
}
