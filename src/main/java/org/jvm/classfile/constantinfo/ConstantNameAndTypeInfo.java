package org.jvm.classfile.constantinfo;

import org.jvm.classfile.ClassReader;
import org.jvm.classfile.ConstantPool;

/**
 * author : wangsixiang02
 * date : 2023/1/7
 * desc :
 */
public class ConstantNameAndTypeInfo implements ConstantInfo {

    private final ConstantPool constantPool;

    private int nameIndex;

    private int descriptorIndex;

    @Override
    public void readInfo(ClassReader classReader) {
        this.nameIndex = classReader.readUint16();
        this.descriptorIndex = classReader.readUint16();
    }

    public ConstantNameAndTypeInfo(ConstantPool constantPool) {
        this.constantPool = constantPool;
    }

    public String getName() {
        return this.constantPool.getUtf8(this.nameIndex);
    }

    public String getDescriptor() {
        return this.constantPool.getUtf8(this.descriptorIndex);
    }
}
