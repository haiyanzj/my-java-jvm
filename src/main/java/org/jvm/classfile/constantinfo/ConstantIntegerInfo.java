package org.jvm.classfile.constantinfo;

import org.jvm.classfile.ClassReader;

/**
 * author : wangsixiang02
 * date : 2023/1/7
 * desc :
 */
public class ConstantIntegerInfo implements ConstantInfo {

    private int val;

    public int getVal() {
        return val;
    }

    @Override
    public void readInfo(ClassReader classReader) {
        this.val = classReader.readUint32();
    }
}
