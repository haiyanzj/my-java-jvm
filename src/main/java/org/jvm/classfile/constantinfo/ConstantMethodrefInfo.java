package org.jvm.classfile.constantinfo;

import org.jvm.classfile.ConstantPool;

/**
 * author : wangsixiang02
 * date : 2023/1/7
 * desc :本类中引用的其他类非接口普通方法
 */
public class ConstantMethodrefInfo extends ConstantMemberrefInfo {


    public ConstantMethodrefInfo(ConstantPool constantPool) {
        super(constantPool);
    }
}
