package org.jvm.classfile.constantinfo;

import org.jvm.classfile.ClassReader;

/**
 * author : wangsixiang02
 * date : 2023/2/18
 * desc :
 */
public class ConstantMethodHandleInfo implements ConstantInfo {
    @Override
    public void readInfo(ClassReader classReader) {
        classReader.readUint8();
        classReader.readUint16();
    }
}
