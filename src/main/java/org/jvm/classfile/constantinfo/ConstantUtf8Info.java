package org.jvm.classfile.constantinfo;

import org.jvm.classfile.ClassReader;

import java.nio.charset.StandardCharsets;

/**
 * author : wangsixiang02
 * date : 2023/1/7
 * desc :字符串常量类
 */
public class ConstantUtf8Info implements ConstantInfo {

    private String val;

    public String getVal() {
        return val;
    }

    @Override
    public void readInfo(ClassReader classReader) {
        int length = classReader.readUint16();
        byte[] res = new byte[length];
        for (int i = 0; i < length; i++) {
            res[i] = (byte) classReader.readUint8();
        }
        this.val = new String(res, StandardCharsets.UTF_8);
    }
}
