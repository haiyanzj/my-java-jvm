package org.jvm.classfile.attributeinfo;

import org.jvm.classfile.ClassReader;

/**
 * 记录方法抛出的异常表
 *
 * @author 海燕
 * @date 2023/1/8
 */
public class ExceptionsAttribute implements AttributeInfo {

    private int[] exceptionIndexTable;

    @Override
    public void readInfo(ClassReader classReader, int attributeLength) {
        this.exceptionIndexTable = classReader.readUint16s();
    }

    public int[] getExceptionIndexTable() {
        return exceptionIndexTable;
    }
}
