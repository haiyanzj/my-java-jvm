package org.jvm.classfile.attributeinfo;

/**
 * 用以标识过时的字段、方法、类
 *
 * @author 海燕
 * @date 2023/1/8
 */
public class DeprecatedAttribute extends MarkerAttribute {

}
