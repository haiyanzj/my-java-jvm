package org.jvm.classfile.attributeinfo;

import org.jvm.classfile.ClassReader;

/**
 * 仅用于标记的属性，没有属性体
 *
 * @author 海燕
 * @date 2023/1/8
 */
public class MarkerAttribute implements AttributeInfo {

    @Override
    public void readInfo(ClassReader classReader, int attributeLength) {
        //由于没有属性体，所以这里不需要读
    }
}
