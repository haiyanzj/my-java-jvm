package org.jvm.classfile.attributeinfo;

import org.jvm.classfile.ClassReader;
import org.jvm.classfile.ConstantPool;

/**
 * 标识源文件名的属性
 *
 * @author 海燕
 * @date 2023/1/8
 */
public class SourceFileAttribute implements AttributeInfo {

    /**
     * 指向常量池中的文件名字符串索引
     */
    private int sourceFileIndex;

    private final ConstantPool constantPool;

    public SourceFileAttribute(ConstantPool constantPool) {
        this.constantPool = constantPool;
    }

    /**
     * @param classReader
     * @param attributeLength 作为一个定长属性，属性长度一定是2
     */
    @Override
    public void readInfo(ClassReader classReader, int attributeLength) {
        this.sourceFileIndex = classReader.readUint16();
    }

    public String fileName() {
        return this.constantPool.getUtf8(sourceFileIndex);
    }
}
