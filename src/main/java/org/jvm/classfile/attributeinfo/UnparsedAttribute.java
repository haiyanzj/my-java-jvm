package org.jvm.classfile.attributeinfo;

import org.jvm.classfile.ClassReader;

/**
 * 本版本虚拟机不支持的属性，仅读，不做解析
 *
 * @author 海燕
 * @date 2023/1/8
 */
public class UnparsedAttribute implements AttributeInfo {

    private String name;

    private byte[] info;

    public UnparsedAttribute(String name) {
        this.name = name;
    }

    @Override
    public void readInfo(ClassReader classReader, int attributeLength) {
        this.info = classReader.readBytes(attributeLength);
    }

    public String getName() {
        return name;
    }

    public byte[] getInfo() {
        return info;
    }
}
