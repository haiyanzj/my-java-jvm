Java实现Java虚拟机，实现类加载的父类委托、方法调用、多线程与锁（初步）、标准输出等功能。参考《自己动手写Java虚拟机》、《Java虚拟机规范》

使用命令行参数启动，jdk版本要求1.8.0

不支持lambda 表达式

-Xjre "填写jre路径" -cp "填写classPath"  填写主类名

例如 -Xjre "/Library/Java/JavaVirtualMachines/jdk1.8.0_361.jdk/Contents/Home/jre" -cp "
/Users/sixiangwang/IdeaProjects/hello/out/production/hello" one.Hello